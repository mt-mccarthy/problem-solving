\documentclass[notitlepage]{../../problem-solving}

\author{Matt McCarthy}
\title{Lebesgue Integration}

\begin{document}

\maketitle

\begin{thm*}
	Let $\Delta:[0,1]\rightarrow\set{0,1}$ be given by
	\[
		\Delta(x) =
		\begin{cases}
			0 & x\in\QQ\\
			1 & x\notin\QQ
		\end{cases}.
	\]
	Then the Lebesgue integral of $\Delta$ on $[0,1]$ is
	\[
		\int_{[0,1]} \Delta\, d\mu = 1.
	\]
\end{thm*}

\section{Introduction}

Consider the function $f:[0,2]\rightarrow\set{0,1}$ given by
\[
	f(x)=
	\begin{cases}
		0 & x < 1\\
		1 & x \geq 1
	\end{cases}.
\]
If we wanted the area under the curve, we can evaluate its integral to find that
\[
	\int_0^2 f(x)dx = 1.
\]
This worked, because the Riemann integral can be ``split'' at points where discontinuities occur, however this can only be taken so far.
For example, consider the map $\Delta:[0,1]\rightarrow\set{0,1}$ given by
\[
	\Delta(x) =
	\begin{cases}
		0 & x\in\QQ\\
		1 & x\notin\QQ
	\end{cases}.
\]
Since $\Delta$ has infinitely many discontinuities, $\Delta$ is \textbf{not} Riemann integrable.
However, if we consider the graph of $\Delta$ in Figure~\ref{d-graph}, we see that it should have a non-zero area under the curve.
Its with this motivation that Lebesgue integration was born.
\begin{figure}[h]
	\centering
	\includegraphics[scale=0.5]{dirichlet.png}
	\caption{Graph of $\Delta(x)$ on $[0,1]$\label{d-graph}}
\end{figure}

\section{Background}

Before we can talk about Lebesgue integration, we need to build up some basic measure theory, starting with the concept of \textit{measurable sets} or a \textit{$\sigma$-algebra}.

\begin{definition}[$\sigma$-algebra]
	Let $X$ be a nonempty set and let $\Sigma\subseteq\mathcal{P}(X)$.
	Then $\Sigma$ is a \textit{$\sigma$-algebra over $X$} if all of the following are true.
	\begin{enumerate}
		\item $X\in\Sigma$.
		\item For all $A\in\Sigma$, $A^c\in\Sigma$.
		\item If $\set{A_i}_{i\in I}$ is a countable collection of sets in $\Sigma$, then $\cup_{i\in I} A_i\in\Sigma$.
	\end{enumerate}
\end{definition}

As a consequence of these basic properties, we get two more properties for free.

\begin{proposition}\label{sigma-prop}
	If $\Sigma$ is a $\sigma$-algebra over $X$, then:
	\begin{enumerate}
		\item $\emptyset\in\Sigma$;
		\item and if $\set{A_i}_{i\in I}$ is a countable collection of sets in $\Sigma$, then $\cap_{i\in I} A_i\in\Sigma$.
	\end{enumerate}
\end{proposition}

An example of a $\sigma$ algebra is the set of all countable unions, countable intersections, and complements of open sets known as the \textit{Borel $\sigma$-algebra}.

\begin{definition}
	The \textit{Borel $\sigma$-algebra} over $\RR$ is defined to be the smallest $\sigma$-algebra containing all of the open sets of $\RR$ and is closed under countable unions, countable intersections, and complements.
\end{definition}
In addition to open intervals, all closed intevals and half-open/half-closed intervals are in the Borel $\sigma$-algebra.
Now that we know what $\sigma$-algebras are, we can talk about measure theory.

\begin{definition}[Measure]
	Let $X$ be a nonempty set, and $\Sigma$ be a $\sigma$-algebra over $X$, and $\mu:\Sigma\rightarrow\RR\cup\set{\infty}$ be a function.
	Then $\mu$ is a \textit{measure} if all of the following are true.
	\begin{enumerate}
		\item For all $E\in\Sigma$, $\mu(E)\geq 0$.
		\item $\mu(\emptyset)=0$.
		\item If $\set{E_i}_{i\in I}$ is a countable collection of pairwise disjoint sets in $\Sigma$, then
		\[
			\mu\paren{\bigcup\limits_{i\in I} E_i}=\sum_{i\in I} \mu(E_i).
		\]
	\end{enumerate}
	Moreover, any $S\in\Sigma$ is said to be \textit{measurable}.
\end{definition}

But wait, there's more!
From the three measure axioms, we pick up a few more properties for free.
\begin{proposition}\label{measure-prop}
	Let $X$ be nonempty, let $\Sigma$ be a $\sigma$-algebra over $X$, and let $\mu$ be a measure on $X$.
	Then:
	\begin{enumerate}
		\item for all $E,F\in\Sigma$, $\mu(E\setminus F) = \mu(E)-\mu(E\cap F)$;
		\item for descending chains $E_1\supset E_2\supset \ldots$,
		\[
			\lim\limits_{n\rightarrow\infty} \mu(E_n) = \mu\paren{\bigcap\limits_{n\rightarrow\infty} E_n}.
		\]
	\end{enumerate}
\end{proposition}
Some examples of measures are the counting measure, length, area, and $n$-dimensional volume.
However, the last three examples are the 1-dimensional, 2-dimensional, and $n$-dimensional Lebesgue measures respectively.
\begin{definition}[Lebesgue Measure]
	Let $\Sigma$ be the Borel $\sigma$-algebra on $\RR$.
	Then the \textit{Lebesgue measure on $\RR$} is the map $\mu:\Sigma\rightarrow\RR\cup\set{\infty}$ given by
	\begin{align*}
		\mu([a,b])&=b-a\\
		\mu(E)&=\inf
		\Bigg\{
		\sum_{i\in I}\mu([a_i,b_i]):
		E\subseteq\bigcup\limits_{i\in I} [a_i,b_i],
		I\text{ countable}
		\Bigg\}.
	\end{align*}
\end{definition}

With the Lebesgue measure defined, we can begin to build up Lebesgue integration.
\begin{definition}
	Let $X$ be a nonempty set and let $A\subseteq X$.
	Then the \textit{characteristic function of $A$} or the \textit{indicator of $A$} is the map $\chi_A:X\rightarrow\set{0,1}$, given by
	\[
		\chi_A(x)=
		\begin{cases}
			1 & x\in A\\
			0 & x\notin A
		\end{cases}.
	\]
\end{definition}
This leads us to the notion of functions represented as sums of characteristic functions.
\begin{definition}
	Let $f:X\rightarrow\RR$ be a function.
	Then $f$ is \textit{simple} if the image of $f$ is finite.
	That is, we can represent $f$ as the sum
	\[
		f(x)=\sum_{i=1}^n a_i\chi_{A_i}(x)
	\]
	where the $a_i$'s are distinct and the $A_i$'s partition $X$.
\end{definition}
We can now define the simplest case of the Lebesgue integral.
\begin{definition}
	The \textit{Lebesgue integral} of a non-negative simple function $f(x)=\sum_{i=1}^n a_i\chi_{A_i}$ is
	\[
		\int f d\mu = \sum_{i=1}^n a_i\mu(A_i)
	\]
	where $\mu$ is the Lebesgue measure.
\end{definition}

\section{Solution}

\begin{thm*}
	Let $\Delta:[0,1]\rightarrow\set{0,1}$ be given by
	\[
		\Delta(x) =
		\begin{cases}
			0 & x\in\QQ\\
			1 & x\notin\QQ
		\end{cases}.
	\]
	Then the Lebesgue integral of $\Delta$ on $[0,1]$ is
	\[
		\int_{[0,1]} \Delta\, d\mu = 1.
	\]
\end{thm*}

To prove this theorem, we will show that $\QQ$ has measure 0 and then use that fact and the simplicity of $\Delta$ to evaluate the Lebesgue integral.
The fastest way to show that $\QQ$ has measure 0, is to show that a singleton has measure 0.

\begin{proposition}
	Let $x\in\RR$, then the set $\set{x}$ is Lebesgue measurable and $\mu(\set{x})=0$.
\end{proposition}
\begin{proof}
	We begin by showing that $\set{x}$ is measurable.
	We know that $\set{x}=\cap_{n=1}^\infty [x, x+1/2^n]$.
	Furthermore, recall that any closed interval is measurable.
	Therefore, by Proposition~\ref{sigma-prop}, $\set{x}$ is measurable.
	Moreover,
	\[
		\mu(\set{x})
		= \lim\limits_{n\rightarrow\infty} \mu\paren{\brac{x, x+\frac{1}{2^n}}}
		=\lim\limits_{n\rightarrow\infty} \frac{1}{2^{n}}
		= 0.
	\]
	Therefore, $\mu(\set{x})=0$.
\end{proof}
\begin{corollary}\label{q-null}
	\[
		\mu(\QQ)=0
	\]
\end{corollary}
\begin{proof}
	Since $\QQ$ is countable, $\QQ=\cup_{n\in\NN} \set{q_n}$.
	Therefore,
	\[
		\mu(\QQ)=\sum_{n\in\NN} \mu(\set{q_n})=0.
	\]
\end{proof}
\begin{upshot*}
	If $x$ is randomly drawn from $[0,1]$, the probability that $x$ is rational is 0.
\end{upshot*}

Now that we know that $\QQ$ is measure 0, we can proceed to the evaluation of the Lebesgue integral.

\begin{thm}
	Let $d:[0,1]\rightarrow\set{0,1}$ be given by
	\[
		\Delta(x) =
		\begin{cases}
			0 & x\in\QQ\\
			1 & x\notin\QQ
		\end{cases}.
	\]
	Then the Lebesgue integral of $\Delta$ on $[0,1]$ is
	\[
		\int_{[0,1]} \Delta\, d\mu = 1.
	\]
\end{thm}
\begin{proof}
	Note that the range of $\Delta$ is finite, and thus $\Delta$ is simple and given by
	\[
		\Delta(x) = \chi_{[0,1]\setminus\QQ}.
	\]
	Since $\Delta$ is simple and nonnegative,
	\[
		\int_{[0,1]} \Delta d\mu = \mu([0,1]\setminus\QQ).
	\]
	However, by Proposition~\ref{measure-prop} and Corollary~\ref{q-null},
	\[
		\int_{[0,1]} \Delta d\mu = \mu([0,1])-\mu([0,1]\cap\QQ) = \mu([0,1]) = 1.
	\]
\end{proof}

\end{document}
