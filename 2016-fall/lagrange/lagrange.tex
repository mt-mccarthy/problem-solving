\documentclass[notitlepage]{../../problem-solving}

\author{Matt McCarthy}
\date{September 2016}
\title{Theorem of Lagrange}

\begin{document}

\maketitle

\begin{thm*}
	Let $G$ be a finite group and $H$ be a subgroup of $G$.
	If $k$ is the number of left cosets of $H$, then
	\[
		|G|=k|H|.
	\]
\end{thm*}

\section{Background}

\subsection{Groups, Subgroups, and Cosets}

In order to talk about any algebraic structure, we need to know how to define an \textit{operation} on a set.

\begin{definition}[Binary Operation]
	Let $*:G\times G\rightarrow G$ be a function.
	Then $*$ is a \textit{binary operation} on $G$ if for all $a,b\in G$, $a*b\in G$.
	We also define $ab:=a*b$.
	If $*$ is a binary operation on $G$, we say $G$ is \textit{closed under $*$}.
\end{definition}

Addition and multiplication are binary operations on $\ZZ$, matrix multiplication is a binary operation on $n\times n$ matrices over a field, and function composition is a binary operation on the set of all functions from a nonempty set $S$ to $S$.
A \textit{group} is a set equipped with a binary operation that satisfies three properties.

\begin{definition}[Group]
	Let $G$ be a nonempty set and let $*:G\times G\rightarrow G$ be a binary operation on $G$.
	Then $G$ is a \textit{group} under $*$ if all of the following are true.
	\begin{enumerate}
		\item For all $a,b,c\in G$, $a(bc)=(ab)c$ (associative law).
		\item There exists a $1\in G$ such that $1a=a1=a$ for all $a\in G$ (identity).
		\item For each $a\in G$, there exists an $a^{-1}\in G$ such that $aa^{-1}=a^{-1}a=1$ (inverse).
	\end{enumerate}
	We say the \textit{order} of a group $G$, denoted $|G|$, is the number of distinct elements in $G$.
\end{definition}

Some examples of groups are $(\ZZ,+)$, $(\QQ\setminus\set{0}, \cdot)$, and permutations of a nonempty set under function composition.
Naturally, the notion of a group leads us to the idea of a \textit{subgroup} or a group within a group.

\begin{definition}[Subgroup]
	Let $G$ be a group under $*$ and let $H\subseteq G$ be nonempty.
	Then $H$ is a \textit{subgroup} of $G$, denoted $H\leq G$, if $H$ is also a group under $*$.
\end{definition}

For example, $2\ZZ=\set{2n | n\in\ZZ}$ is a subgroup of $(\ZZ,+)$.
Furthermore, when we have a subgroup we can produce other sets related to the subgroup by multiplying elements from the parent group to them.
This is how we construct a \textit{coset} of a subgroup.

\begin{definition}[Coset of a Subgroup]
	Suppose $G$ is a group, $H\leq G$, and $g\in G$.
	Then the \textit{left coset of $H$ represented by $g$} is defined as
	\[
		gH := \set{gh | h\in H}.
	\]
	Each element of $gH$ is called a \textit{representative of $gH$}.
\end{definition}

Following our above example, the cosets of $2\ZZ$ are $\set{0+2\ZZ, 1+2\ZZ}$.
From here we can say that elements of $2\ZZ$ are even and elements of $1+2\ZZ$ are odd.
By doing this, we found a type of \textit{equivalence} between integers.

\subsection{Equivalence Relations and Partitions}

Specifically, the previously mentioned equivalence is called an \textit{equivalence relation}.

\begin{definition}[Equivalence Relation]
	Let $S$ be a nonempty set and let $\sim\subseteq S\times S$.
	We say that for any $s,r\in S$, $s\sim r$ if and only if $(s,r)\in\sim$.
	Then $\sim$ is an \textit{equivalence relation} if all of the following are true.
	\begin{enumerate}
		\item For all $s\in S$, $s\sim s$.
		\item For all $s,r\in S$, if $s\sim r$, then $r\sim s$.
		\item For all $s,r,q\in S$, if $s\sim r$ and $r\sim q$, then $s\sim q$.
	\end{enumerate}
\end{definition}

Examples of equivalence relations are, equality, remainder modulo $n$, and having equal absolute value.
With an equivalence relation in hand, we can look at the set of everything equivalent to an element.
This is called an \textit{equivalence class}.

\begin{definition}[Equivalence Classes]
	Suppose $\sim$ is an equivalence relation on a nonempty set $S$.
	Then for any $s\in S$, the \textit{equivalence class containing $s$} is defined as
	\[
		\Pi_\sim(s) = \set{r\in S | s\sim s}.
	\]
\end{definition}

If our equivalence relation is $\sim$ where $a\sim b$ iff $|a|=|b|$ for any $a,b\in\RR$, then $\Pi_\sim(a)=\set{a,-a}$ for each nonzero $a\in\RR$.
We now want to be able to split a set into disjoint subsets, that is we want to \textit{partition} $S$ into nonoverlapping blocks.

\begin{definition}[Partition]
	Let $S$ be a nonempty set and $\mathcal{C}=\set{C_i}_{i\in I}\subseteq \mathcal{P}(S)$.
	We say that $\mathcal{C}$ is a \textit{partition of $S$} if
	\[
		S = \bigcup\limits_{i\in I} C_i
	\]
	and $C_i\cap C_j = \emptyset$ for all $i\neq j$.
\end{definition}

As it turns out, the equivalence classes of an equivalence relation on $S$, partition $S$.

\begin{proposition}
	Equivalence classes of an equivalence relation on a set partition that set.
\end{proposition}

\section{Solution}

\begin{thm*}
	Let $G$ be a finite group and $H$ be a subgroup of $G$.
	If $k$ is the number of left cosets of $H$, then
	\[
		|G|=k|H|.
	\]
\end{thm*}

We begin by showing that being in the same coset forms an equivalence relation on a group.

\begin{lemma}\label{l1}
	Let $G$ be a group and let $H\leq G$.
	Define $\sim:=\set{(g,h) | g\in hH}\subseteq G\times G$.
	Then, $\sim$ is an equivalence relation on $G$.
\end{lemma}
\begin{proof}
	Let $a,b,c\in G$.
	Then, $a=a\cdot 1 \in aH$ and $a\sim a$.
	Suppose $a\sim b$.
	Then, $a\in bH$ and there exists an $h\in H$ such that $a=bh$.
	Therefore, $b=ah^{-1}\in aH$ and $b\sim a$.
	Suppose $a\sim b$ and $b\sim c$.
	Then, $a\in bH$ and $b\in cH$.
	Thus, there exist $h,i\in H$ such that $a=bh$ and $b=ci$.
	Therefore, $a=bh=c(ih)\in cH$ and $a\sim c$.
	Therefore, $\sim$ is an equivalence relation on $G$.
\end{proof}

We then claim that each equivalence class is a coset of $H$.

\begin{lemma}\label{l2}
	Let $G$ be a group, let $H\leq G$, and let $g\in G$.
	Then $\Pi_\sim(g)=gH$.
\end{lemma}
\begin{proof}
	Fix $g\in G$.
	Let $x\in\Pi_\sim(g)$.
	Then $x\sim g$ and $x\in gH$ by definition of $\sim$.
	Suppose $x\in gH$, then $x\sim g$ and $x\in\Pi_\sim(g)$.
	Therefore, $\Pi_\sim(g)= gH$.
\end{proof}

Next, we show that each coset is the same size.

\begin{lemma}\label{l3}
	Let $G$ be a group and let $H\leq G$.
	Then for all $g\in G$, $|gH|=|H|$.
\end{lemma}
\begin{proof}
	Define $f:H\rightarrow gH$ by $f(h)=gh$.
	Suppose $f(h)=f(i)$.
	Then, $gh=gi$ and $h=i$.
	Therefore $f$ is injective.
	Let $gh\in gH$.
	Then, $gh = f(h)$ and $f$ is surjective.
	Therefore $f$ is bijective and $|gH|=|H|$.
\end{proof}

With all of these lemmas, the proof of the Theorem of Lagrange is straightforward.

\begin{thm}[Theorem of Lagrange]
	Let $G$ be a finite group and $H$ be a subgroup of $G$.
	If $k$ is the number of left cosets of $H$, then
	\[
		|G|=k|H|.
	\]
\end{thm}
\begin{proof}
	Since $\sim$ from Lemma~\ref{l1} is an equivalence relation, we know that $G$ is partitioned by
	\[
		\mathcal{P}=\set{\Pi_\sim(g)}_{g\in G}.
	\]
	Furthermore, from Lemma~\ref{l2}, we know that $\Pi_\sim(g)=gH$ and that
	\[
		\mathcal{P}=\set{gH}_{g\in G}.
	\]
	Moreover, since $G$ is partitioned by $\mathcal{P}$,
	\[
		G=\dot{\bigcup\limits_{gH\in\mathcal{P}}} gH.
	\]
	However, $G$ is finite, therefore
	\[
		|G|=\sum_{gH\in\mathcal{P}} |gH|.
	\]
	By Lemma~\ref{l3}, we know that for all $g\in G$, $|gH|=|H|$.
	Ergo,
	\[
		|G|=\sum_{gH\in\mathcal{P}} |H| = |\mathcal{P}||H|.
	\]
	Since each left coset corresponds to an equivalence class (Lemma~\ref{l2}), $|\mathcal{P}|$ is the number of left cosets of $H$.
\end{proof}

\end{document}
