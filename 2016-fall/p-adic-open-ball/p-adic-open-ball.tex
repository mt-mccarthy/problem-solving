\documentclass[notitlepage]{../../problem-solving}

\title{Open Balls in $\QQ_p$}
\date{August 2016}
\author{Matt McCarthy}

\addbibresource{p-adic-open-ball.bib}
\nocite{*}

\begin{document}

\maketitle

\begin{thm*}
	Let $x\in\QQ_p$ and $r\in\RR$ with $r\geq 0$.
	Then for any $y\in B(x,r)$, $B(x,r)=B(y,r)$.
\end{thm*}

\section{Background}

We begin by constructing the $p$-adic numbers, denoted $\QQ_p$.
In order to do so, we need to present a way to talk about ``distance''.

\subsection{Formalization of Distance}

We formally define the distance between two points in a space using a \textit{metric}.

\begin{definition}[Metric Space]
	Let $X$ be a non-empty set and let $d:X\times X\rightarrow \RR$ be a function.
	Then $d$ is a \textit{metric} on $X$ if all of the following hold.
	\begin{enumerate}
		\item For all $x,y\in X$, $d(x,y)\geq 0$.
		\item For all $x,y\in X$, $d(x,y) = 0$ iff $x=y$.
		\item For all $x,y\in X$, $d(x,y)=d(y,x)$.
		\item For all $x,y,z\in X$, $d(x,z)\leq d(x,y)+d(y,z)$ (triangle inequality).
	\end{enumerate}
	If $d$ is a metric on $X$, then we say $(X,d)$ forms a \textit{metric space}.
\end{definition}

Additionally, we define an \textit{open ball} in a metric space as follows.
\begin{definition}[Open Ball]
	Let $(X,d)$ be a metric space, let $x\in X$, and let $r\in\RR$ with $r\geq 0$.
	Then the \textit{open ball of radius $r$ centered at $x$} is defined as,
	\[
		B(x,r) := \set{y\in X | d(x,y) < r}.
	\]
\end{definition}

Armed with the ability to consider distances in a space, we can determine if a given sequence \textit{converges} in that space.
\begin{definition}[Convergent Sequence]
	Let $(X,d)$ be a metric space and let $(a_n)_{n\in\NN}\subseteq X$ and let $a\in X$.
	We say $a_n$ is a \textit{converges} to $a$ if for all $\varepsilon>0$, there exists an $N\in\NN$ such that $d(a_n,a)<\varepsilon$ for all $n\geq N$.
	If such an $a$ exists, we say $a_n$ is \textit{convergent in $X$}.
\end{definition}

If a sequence converges, then its terms will also get arbitrarily close to each as we progress through the sequence; this kind of sequence is called a \textit{Cauchy sequence}.
\begin{definition}[Cauchy Sequence]
	Let $(X,d)$ be a metric space and let $(a_n)_{n\in\NN}\subseteq X$.
	We say $a_n$ is a \textit{Cauchy sequence} if for all $\varepsilon>0$, there exists an $N\in\NN$ such that $d(a_n,a_m)<\varepsilon$ for all $n,m\geq N$.
\end{definition}

Even though convergent implies Cauchy, the converse is not necessarily true.
For example in $\QQ$ under the Euclidean metric ($d(x,y)=|x-y|$), the sequence defined by $(1+1/n)^n$ is Cauchy but not convergent.
However, if we move to $\RR$, $(1+1/n)^n$ converges to $e$.
As a result, we can think of Cauchy sequences as sequences that \textit{should} converge in our space.
If they are not, then we need to move to what is called the \textit{completion} of the metric space.
\begin{definition}[Complete Metric Space]
	Let $(X,d)$ be a metric space.
	We say $X$ is \textit{complete} if all Cauchy sequences in $X$ converge in $X$.
\end{definition}
\begin{thm}
	Let $(X,d)$ be a metric space.
	Then $X$ has a unique completion, $C(X,d)$, up to isometry.
	Furthermore, this completion is isometric to the space $(B(X),D)$ where $B(X)$ is the set of all bounded functions from $X$ to $\RR$, and $D(f,g)=\sup\limits_{x\in X} |f(x)-g(x)|$.
\end{thm}

For example, $\RR$ is the completion of $\QQ$ with respect to the Euclidean metric and $\QQ_p$ is the completion of $\QQ$ with respect to the $p$-adic metric.

\subsection{The $p$-adic Numbers}

Before we can define the $p$-adic metric, we need to introduce the $p$-adic ordinal and $p$-adic absolute value.
\begin{definition}[$p$-adic Ordinal]
	Let $p$ be a prime and let $a\in\ZZ$ be nonzero.
	Then the \textit{$p$-adic ordinal} of $a$, denoted $\ord_p a$, is defined as
	\[
		\ord_p a = \max\set{n\,:\, p^n | a}.
	\]
	Furthermore, for any nonzero $x=b/c\in\QQ$,
	\[
		\ord_p x = \ord_p a - \ord_p b.
	\]
\end{definition}

Using the definition of $p$-adic ordinal, we now provide the definition of the $p$-adic absolute value.
\begin{definition}[$p$-adic absolute value]
	Let $p$ be a prime and $x\in\QQ$.
	Then the \textit{$p$-adic absolute value of $x$}, denoted $|x|_p$, is defined as
	\[
		|x|_p =
		\begin{cases}
			p^{-\ord_p x} & x\neq 0\\
			0 & x=0.
		\end{cases}
	\]
\end{definition}

While an absolute value is not a metric in and of itself, we can use it to generate a metric.
Similarly to how we define $|x-y|$ to be the Euclidean metric, we define $|x-y|_p$ as the $p$-adic metric.
With this, we can create the $p$-adic numbers.
\begin{definition}[$p$-adic Numbers]
	The set of \textit{$p$-adic numbers}, denoted $\QQ_p$, is the completion of the metric space $(\QQ,|\cdot|_p)$.
\end{definition}

The $p$-adic numbers share many properties with $\RR$.
For example, they are both totally ordered fields and they are both uncountably infinite.
However, their orderings are entirely different.
In $\RR$, $2 < 4$, but in $\QQ_2$, $4 < 2$ since $|4|_2 = 1/4 < 1/2 = |2|_2$.

Additionally, the $p$-adic metric satisfies a much stronger version of the triangle inequality.
\begin{thm}[Strong Triangle Inequality]
	For all $x,y\in\QQ_p$, $|x+y|_p\leq\max\set{|x|_p,|y|_p}$.
\end{thm}

\section{Solution}

\begin{thm*}
	Let $x\in\QQ_p$ and $r\in\RR$ with $r\geq 0$.
	Then for any $y\in B(x,r)$, $B(x,r)=B(y,r)$.
\end{thm*}

We begin by showing that the open ball of radius $r$ centered at any point in $B(x,r)$ is contained within $B(x,r)$.
\begin{lemma}\label{master-ball}
	Let $x\in\QQ_p$ and $r\in\RR$ with $r\geq 0$.
	Then for any $y\in B(x,r)$, $B(y,r)\subseteq B(x,r)$.
\end{lemma}
\begin{proof}
	We know that since $y\in B(x,r)$, $|x-y|_p< r$.
	Let $z\in B(y,r)$.
	Then we know that $|y-z|_p< r$.
	Consider $|x-z|_p$.
	\begin{align*}
		|x-z|_p &= |(x-y) + (y-z)|_p\\
		&\leq \max\set{|x-y|_p, |y-z|_p}\\
		&< r
	\end{align*}
	Thus, $z\in B(x,r)$ and $B(y,r)\subseteq B(x,r)$.
\end{proof}
From here, the proof is fairly trivial.
\begin{thm}\label{one-ball-to-rule-them-all}
	Let $x\in\QQ_p$ and $r\in\RR$ with $r\geq 0$.
	Then for any $y\in B(x,r)$, $B(x,r)=B(y,r)$.
\end{thm}
\begin{proof}
	Since $y\in B(x,r)$, $B(y,r)\subseteq B(x,r)$ by Lemma~\ref{master-ball}.
	Moreover, since $y\in B(x,r)$, we know that $|x-y|_p<r$ and thus $x\in B(y,r)$.
	Therefore by Lemma~\ref{master-ball}, $B(x,r)\subseteq B(y,r)$.
	Ergo, $B(x,r)=B(y,r)$.
\end{proof}

\subsection{$n$-Dimensional Extension}

You may have noticed that an open ball in $\QQ_p$ is just an open interval.
This result can easily be extended to $\paren{\QQ_p}^n$ by defining the following \textit{norm} on $\paren{\QQ_p}^n$,
\[
	||x||_p := \max\limits_{1\leq k \leq n}\set{|x_k|_p}.
\]

We can then use this norm as a metric by defining the distance between $x$ and $y$ to be $||x-y||_p$.
Furthermore, this norm still satisfies the strong triangle inequality.
\begin{proposition}
	For any $x,y\in(\QQ_p)^n$,
	\[
		||x+y||_p \leq\max\set{||x||_p,||y||_p}.
	\]
\end{proposition}
\begin{proof}
	\begin{align*}
		||x+y||_p &= \max\limits_{1\leq k\leq n}\set{|x_k+y_k|_p}\\
		&\leq\max\limits_{1\leq k\leq n}\set{|x_k|_p, |y_k|_p}\\
		&=\max\Big\lbrace\max\limits_{1\leq k\leq n}\set{|x_k|_p},\max\limits_{1\leq k\leq n}\set{|y_k|_p}\Big\rbrace\\
		&=\max\set{||x||_p,||y||_p}
	\end{align*}
\end{proof}
From here, we get the following lemma whose proof works exactly like Lemma~\ref{master-ball}.
\begin{lemma}\label{master-ball-n}
	Let $x\in(\QQ_p)^n$ and $r\in\RR$ such that $r\geq 0$.
	Then for any $y\in B(x,r)$, $B(y,r)\subseteq B(x,r)$.
\end{lemma}

Additionally, we can invoke Lemma~\ref{master-ball-n} twice to show that the two open balls are equal just as we did in the proof for \autoref{one-ball-to-rule-them-all}.
\begin{thm}
	Let $x\in (\QQ_p)^n$ and $r\in\RR$ such that $r\geq 0$.
	Then for any $y\in B(x,r)$, $B(x,r)=B(y,r)$.
\end{thm}

Thus, any point in a ball in $n$-dimensional $p$-adic space is the center of that ball.

\end{document}
