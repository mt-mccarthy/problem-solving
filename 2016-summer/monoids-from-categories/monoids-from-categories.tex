\documentclass[notitlepage]{problem-solving}

\usepackage{mathtools}

\author{Matt McCarthy}
\date{June 2016}
\title{Generating Monoids from Categories}

\addbibresource{monoids-from-categories.bib}
\nocite{*}

\begin{document}

\maketitle

\begin{thm*}
	Let $C$ be a category and let $A$ be an object in $C$.
	Then $C[A,A]$ forms a monoid under arrow composition.
\end{thm*}

\section{Background}

We begin by providing a definition of a category.

\begin{definition}[Category]
	A \textit{category}, $C$, consists of the following.
	\begin{enumerate}
		\item A class of \textit{objects}, denoted $Obj(C)$.
		\item A class of \textit{arrows}, denoted $Arr(C)$.
		Each arrow $f\in Arr(C)$ has a source object $A\in Obj(C)$, a target object $B\in Obj(C)$, and is denoted $f:A\rightarrow B$.
		We denote the class of all arrows going from $A\in Obj(C)$ to $B\in Obj(C)$ as $C[A,B]$.
		\item A partial composition $\circ:Arr(C)\times Arr(C)\rightarrow Arr(C)$ such that for any $f:A\rightarrow B,g:B\rightarrow D$, $gf:A\rightarrow D\in Arr(C)$.
	\end{enumerate}
	Furthermore, the following axioms must hold.
	\begin{enumerate}
		\item For all $f:A\rightarrow B,g:B\rightarrow D,h:D\rightarrow E\in Arr(C)$, $h(gf) = (hg)f$.
		\item For all $A\in Obj(C)$, there exists an $id_A\in C[A,A]$ such that for all arrows $f:B\rightarrow A$, $g:A\rightarrow D$ $id_A\, f =f$ and $g\, id_A = g$.
	\end{enumerate}
\end{definition}

Some examples of categories are given in the table below.
\begin{figure}[h]
	\centering
	\begin{tabular}{|c|c|c|}
		\hline
		\textbf{Name} & \textbf{Objects} & \textbf{Arrows}\\\hline
		\textit{Grp} & Groups & Group homomorphisms\\\hline
		\textit{Top} & Topological Spaces & Continuous maps\\\hline
		$Vect_\KK$ & Vector spaces over a field $\KK$ & Linear transformations\\\hline
		\textit{Pos} & Posets & Monotone maps\\\hline
		\textit{RelA} & Sets & Binary relations\\\hline
	\end{tabular}
	\caption{Examples of categories \cite{simmons}}
\end{figure}

We now define monoids.
\begin{definition}[Monoid]
	Let $M$ be a set, and let $*:M^2\rightarrow M$ be a binary operation.
	Then $(M,*)$ forms a \textit{monoid} if all of the following are satisfied.
	\begin{enumerate}
		\item For all $a,b,c\in M$, $a*(b*c)=(a*b)*c$.
		\item There exists an $e\in M$ such that for all $a\in M$, $e*a=a*e=a$.
	\end{enumerate}
\end{definition}

\section{Solution}

\begin{thm}
	Let $C$ be a category and let $A$ be an object in $C$.
	Then $C[A,A]$ forms a monoid under arrow composition.
\end{thm}
\begin{proof}
	Let $f,g\in C[A,A]$.
	Then
	\[
		A \xrightarrow{f} A \xrightarrow{g} A
	\]
	and thus
	\[
		A \xrightarrow{fg} A.
	\]
	Therefore, arrow composition forms a binary operation on $C[A,A]$.

	Next, we claim that $id_A$ is the identity for $C[A,A]$ with respect to arrow composition.
	Let $f\in C[A,A]$.
	Then, by definition, we know that $f\, id_A = id_A\, f = f$.
	Thus, $id_A$ is the identity for $C[A,A]$ with respect to arrow composition.

	Furthermore, arrow composition is defined to be associative.
	Thus, $C[A,A]$ forms a monoid under arrow composition.
\end{proof}

\printbibliography

\end{document}
